from nonebot import on_notice, NoticeSession

bot = nonebot.get_bot()


# 将函数注册为群成员增加通知处理器
@on_notice('group_increase')
async def increase(session: NoticeSession):
    # 发送欢迎消息
    q = session.ctx['user_id']
    await session.send(f'😀欢迎新朋友～ [CQ:at,qq={q}]')


@on_notice('group_decrease')
async def decrease(session: NoticeSession):
    # 发送消息
    q = str(session.ctx['user_id'])
    m = str(session.ctx['operator_id'])
    if m == q:
        inf = await bot.get_stranger_info(user_id=q)
        name = inf['nickname']
        await session.send(f'{name}({q}) 跑了')

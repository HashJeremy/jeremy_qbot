import requests


async def get_lua_version_of_mod(mod: str) -> str:
    # 这里简单返回一个字符串
    # 实际应用中，这里应该调用返回真实数据的天气 API，并拼接成天气预报内容
    headers = {"Host": "erp.openluat.com",
               'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
               'Accept': '*/*',
               'Referer': 'http://erp.openluat.com/luat',
               'Accept-Encoding': 'gzip, deflate',
               'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6',
               'Cookie': 'UM_distinctid=170af798b2ea5-031b5502f8390a-4313f6a-e1000-170af798b2f5aa; remember_token=16311|0e034cc6c87585c25a62cbf6343d1a479bfb01e99d771a4b2339ab6147bc16fb4e4bb33affe46db824661b26eecaea901b6b14cdd9c66e1f8a06d4f5f7a0e246; Hm_lvt_b73e49f8578cedfcb3c39e6e8db9d782=1585993850,1585995964,1586169013,1586425644; erp session=94301168-bd5d-4137-b234-3f8d05fb1d90.keQYllnN3wxbKmfwC76cAPy0Ca0'}
    if mod == '1802':
        r = requests.get(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802&usage=&test_status=&software_type_2=LUAT',
            headers=headers)
        pure_res = r.json()['data']
        first = pure_res[0]
        for version in pure_res:
            version_type = version['usage']
            if version_type == '正式版本' or version_type == '量产版本':
                first = version
                break
        return_msg = '版本名称：' + first['name'] + '\r\n' + '版本号：' + first['core_version'] + '\r\n' + '创建时间：' + first[
            'creation_time'] + '\r\n' + '适用产品：' + first['product_names'] + '\r\n' + '版本状态：' + first[
                         'usage'] + '\r\n' + '版本说明：' + first['info'] + '\r\n' + '创建人：' + first[
                         'creator'] + '\r\n' + '下载地址：' + first['software_url'] + '\r\n'
        return return_msg
    elif mod == '1802S' or mod == '1802s':
        r = requests.get(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802S&usage=&test_status=&software_type_2=LUAT',
            headers=headers)
        pure_res = r.json()['data']
        first = pure_res[0]
        for version in pure_res:
            version_type = version['usage']
            if version_type == '正式版本' or version_type == '量产版本':
                first = version
                break
        return_msg = '版本名称：' + first['name'] + '\r\n' + '版本号：' + first['core_version'] + '\r\n' + '创建时间：' + first[
            'creation_time'] + '\r\n' + '适用产品：' + first['product_names'] + '\r\n' + '版本状态：' + first[
                         'usage'] + '\r\n' + '版本说明：' + first['info'] + '\r\n' + '创建人：' + first[
                         'creator'] + '\r\n' + '下载地址：' + first['software_url'] + '\r\n'
        return return_msg
    elif mod == '8910':
        r = requests.get(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=8910&usage=&test_status=&software_type_2=LUAT',
            headers=headers)
        pure_res = r.json()['data']
        first = pure_res[0]
        for version in pure_res:
            version_type = version['usage']
            if version_type == '正式版本' or version_type == '量产版本':
                first = version
                break
        return_msg = '版本名称：' + first['name'] + '\r\n' + '版本号：' + first['core_version'] + '\r\n' + '创建时间：' + first[
            'creation_time'] + '\r\n' + '适用产品：' + first['product_names'] + '\r\n' + '版本状态：' + first[
                         'usage'] + '\r\n' + '版本说明：' + first['info'] + '\r\n' + '创建人：' + first[
                         'creator'] + '\r\n' + '下载地址：' + first['software_url'] + '\r\n'
        return return_msg
    else:
        return '没有'

'''
Author:Jeremy
'''
from nonebot import on_command, CommandSession
from .data_source import get_lua_version_of_mod


# on_command 装饰器将函数声明为一个命令处理器
# 这里 version 为命令的名字，同时允许使用别名
@on_command('lua', aliases=('LUA', 'Lua'))
async def version(session: CommandSession):
    mod = session.get('mod', prompt='查哪个？')
    version_report = await get_lua_version_of_mod(mod)
    await session.send(version_report)


@version.args_parser
async def _(session: CommandSession):
    # 去掉消息首尾的空白符
    stripped_arg = session.current_arg_text.strip()

    if session.is_first_run:
        # 该命令第一次运行（第一次进入命令会话）
        if stripped_arg:
            session.state['mod'] = stripped_arg
        return

    if not stripped_arg:
        session.pause('输的不对')

    session.state[session.current_key] = stripped_arg

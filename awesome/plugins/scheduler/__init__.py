import nonebot
import requests
from aiocqhttp.exceptions import Error as CQHttpError

bot = nonebot.get_bot()
headers = {"Host": "erp.openluat.com",
           'Connection': 'keep-alive',
           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
           'Accept': '*/*',
           'Referer': 'http://erp.openluat.com/luat',
           'Accept-Encoding': 'gzip, deflate',
           'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6',
           'Cookie': 'UM_distinctid=170af798b2ea5-031b5502f8390a-4313f6a-e1000-170af798b2f5aa; remember_token=16311|0e034cc6c87585c25a62cbf6343d1a479bfb01e99d771a4b2339ab6147bc16fb4e4bb33affe46db824661b26eecaea901b6b14cdd9c66e1f8a06d4f5f7a0e246; Hm_lvt_b73e49f8578cedfcb3c39e6e8db9d782=1585993850,1585995964,1586169013,1586425644; erp session=94301168-bd5d-4137-b234-3f8d05fb1d90.keQYllnN3wxbKmfwC76cAPy0Ca0'}


async def search_new_version(url, filename):
    r = requests.get(url, headers=headers)
    pure_res = r.json()['data']
    first = pure_res[0]
    for version in pure_res:
        version_type = version['usage']
        if version_type == '正式版本' or version_type == '量产版本':
            first = version
            break
    now_ver = first['core_version'].replace('V', '')
    with open("./awesome/plugins/scheduler/" + filename + ".txt", 'r+') as f:
        tmp = f.read()
        if int(now_ver) > int(tmp):
            f.seek(0)
            f.truncate()  # 清空文件
            f.write(now_ver)
            notify_msg = '检测到' + filename + '正式版本升级'
            await bot.send_group_msg(group_id=132624122, message=notify_msg)
            await bot.send_private_msg(user_id=734592627, message=notify_msg)
            version_msg = '版本名称：' + first['name'] + '\r\n' + '版本号：' + first['core_version'] + '\r\n' + '创建时间：' + \
                          first[
                              'creation_time'] + '\r\n' + '适用产品：' + first['product_names'] + '\r\n' + '版本状态：' + \
                          first[
                              'usage'] + '\r\n' + '版本说明：' + first['info'] + '\r\n' + '创建人：' + first[
                              'creator'] + '\r\n' + '下载地址：' + first['software_url'] + '\r\n'
            await bot.send_group_msg(group_id=132624122, message=version_msg)
            await bot.send_private_msg(user_id=734592627, message=version_msg)


@nonebot.scheduler.scheduled_job('interval', minutes=5)
async def _():
    try:
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802&usage=&test_status=&software_type_2=AT',
            '1802_AT')
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802S&usage=&test_status=&software_type_2=AT',
            '1802s_AT')
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=8910&usage=&test_status=&software_type_2=AT',
            '8910_AT')
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802&usage=&test_status=&software_type_2=LUAT',
            '1802_LUA')
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=1802S&usage=&test_status=&software_type_2=LUAT',
            '1802s_LUA')
        await search_new_version(
            'http://erp.openluat.com/api/site/product_software?search=&software_type=8910&usage=&test_status=&software_type_2=LUAT',
            '8910_LUA')
    except CQHttpError:
        pass

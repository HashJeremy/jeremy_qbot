# jeremy_qbot

#### 介绍
Jeremy_QBot V1.0

#### 软件架构
+ Python:3.8.X
+ nonebot:1.5.0

#### 目前实现功能
+ 查询Luat模块最新正式AT/Lua版本信息，使用方法举例：
  + at 1802
  + lua 1802S
+ 检查到版本更新自动通知指定用户或群：
  + 这个得改代码
